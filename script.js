let firstNum = +prompt('Введите первое число?');
let secondNum = +prompt('Введите второе число?');
let operand = prompt('Введите операцию?');

while (firstNum === '' || isNaN(firstNum) || secondNum === '' || isNaN(secondNum)) {
    firstNum = +prompt('Введите первое число?', firstNum);
    secondNum = +prompt('Введите второе число?', secondNum);
}

let add = (a, b) => a + b;
let minus = (a, b) => a - b;
let mul = (a, b) => a * b;
let divide = (a, b) => a / b;

let calculate = () => {
    switch (operand) {
        case '+':
            console.log('Result: ' + add(firstNum, secondNum));
            break;
        case '-':
            console.log('Result: ' + minus(firstNum, secondNum));
            break;
        case '*':
            console.log('Result: ' + mul(firstNum, secondNum));
            break;
        case '/':
            console.log('Result: ' + divide(firstNum, secondNum));
            break;
    }
};

calculate();